This is an analysis of Path of Exile: Betrayal's economy based on some of its items.

The database is hosted privately on Kaggle and the data is taken and recorded from the Public Stash API hosted by Grinding Gear Games.

To peruse the document, visit it [on Kaggle](https://www.kaggle.com/brzrkr/analysis-of-path-of-exile-betrayal-s-economy) (and feel free to upvote it ;) ).